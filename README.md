# Развёртывание решения для Remote Development на виртуалке

Инструкция предназначена для тестирования решения с имитацией приватной сети без доступа в интернет (нужные файлы скачиваются отдельно и передаются в приватную сеть каким-либо образом)

## Необходимые компоненты

1. Любая платформа виртуализации - __VMWare__, __VirtualBox__
1. Дистрибутив Linux. В примере используется __CentOS 7__ - <https://mirror.yandex.ru/centos/7/isos/x86_64/CentOS-7-x86_64-Minimal-2003.iso>
1. Дистрибутив __Visual Studio Code 64 bit__ - <https://code.visualstudio.com/download>
1. Скачанные в формате _.vsix_ расширения для VSCode (ссылка 'Download Extension' справа):
    * __Remote - SSH__ - <https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-ssh>
    * __Remote - SSH: Editing Configuration Files__ - <https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-ssh-edit>
    * __Scala Syntax (official)__ - <https://marketplace.visualstudio.com/items?itemName=scala-lang.scala>
1. Скачанный архив с предустановленными расширениями для VSCode Server - extensions.tar.gz (расположен рядом с README.md)

## Установка Linux

1. Создайте виртуалку:
    * CPU - 2
    * RAM - 8Gb
    * Сеть - NAT. Сконфигурировать проброс портов для SSH соединения
    * CDROM - указать iso с CentOS
1. Установить CentOS на виртуалку (инструкцию ищите в интернете) - на забудьте создать кастомного пользователя для SSH коннекта

## Настройка Linux

### Под su

1. 
```
yum -y update
yum -y install epel-release
yum -y install yum-utils net-tools vim wget
```
2. Установите git версии 2 (по умолчанию в CentOS 7 git версии 1) - <https://computingforgeeks.com/how-to-install-latest-version-of-git-git-2-x-on-centos-7/>
1. Установите docker:
    1. <https://docs.docker.com/engine/install/centos/>
    1. <https://docs.docker.com/engine/install/linux-postinstall/> - пункты 'non-root user' и 'start on boot'
1. Установите __Python 3.7__ (по умолчанию 2.7) - у меня толком не получилось, все инструкции указывают, что надо компилировать из исходников. Можно поставить 3.6, но в нашем Confluence я постоянно натыкаюсь, что используемая версия 3.7
1. 
```
chmod 777 /opt
```

### Под кастомным пользователем

1. Установите JDK 1.8
1. Скачайте архив tgz __sbt__ ( берите последнюю версию, https://github.com/sbt/sbt/releases/latest )
```
wget https://github.com/sbt/sbt/releases/download/v1.4.3/sbt-1.4.3.tgz
```
3. Скачайте архив tgz __Spark 2.3.0__
```
wget https://archive.apache.org/dist/spark/spark-2.3.0/spark-2.3.0-bin-hadoop2.6.tgz
```
4. (Опционально)_ Скачайте архив tgz __Spark 3.0__
```sh
wget https://apache-mirror.rbc.ru/pub/apache/spark/spark-3.0.1/spark-3.0.1-bin-hadoop2.7.tgz
```
5. Распакуйте архивы в /opt
1. Пропишите в .bash_profile переменные окружения и PATH к установленным дистрибутивам
```
SBT_HOME=/opt/sbt
SPARK_HOME=/opt/spark-2.3.0-bin-hadoop2.6
PATH=$SBT_HOME/bin:$SPARK_HOME/bin:$PATH:$HOME/.local/bin:$HOME/bin
```
7. 
```sh
. .bash_profile
```
8. Создайте временную директорию, перейдите туда и ротестируйте установленные дистрибутивы
```sh
# Должна отобразиться версия java 1.8.0_xxx
java -version
# Должна отобразиться версия sbt 1.4.1 (запуск sbt небыстрый, наберитесь терпения + при первом запуске будут качаться некоторые библиотеки из инета)
# При запуске sbt создаются директории project и target, можно их удалить
sbt -version
# Должна отобразиться версия python 3.7
python --version
# Должна отобразиться версия git 2.x.x
git --version
# Должен запусться dsocker container hello-world
docker run hello-world
# Вы должны попасть в командную оболочку Spark 2.3.0
spark-shell
```

## Настройка хостовой машины

1. Создайте пару ключей для кастомного пользователя на виртуальном хосте для SSH аутентификации - <https://evilinside.ru/nastrojka-ssh-avtorizacii-po-klyuchu-bez-parolya-v-centos-7/>#
1. Скачайте закрытый ключ на хостовую машину, настройте C:\Users\\{local_login}\\.ssh\config на хостовой машине
```sh
Host vscode-vm
    HostName {host_ip}
    Port {forward_port}
    User {vm_login}
    IdentityFile C:\Users\{local_login}\.ssh\id_rsa
```
3. Протестируйте ssh соединение с виртуальным хостом
```sh
ssh {vm_login}@{host_ip} -p {forward_port}
```

## Настройка VSCode

1. Открыть инструкцию <https://code.visualstudio.com/docs/remote/ssh#_connect-to-a-remote-host>
1. Открыть инструкцию <https://medium.com/@debugger24/installing-vscode-server-on-remote-machine-in-private-network-offline-installation-16e51847e275>
1. __!!!__ Для чистоты тестирования отключите сетевое соединение на хосте
1. Установите VSCode из скачанного дистрибутива, запустить его
1. Вручную установите _.vsix_ расширения для VSCode (EXTENSIONS -> More Actions... -> Install from VSIX)
1. Из VSCode установите соединение к vscode-vm - см. 1-ю инструкцию
1. При установке соединения должна возникнуть ошибка (если её нет, значит вы забыли отключить сеть). Ваши следующие действия:
    * включаем сеть
    * скачиваем на виртуальный хост дистрибутив vscode-srver согласно 2-й инструкции
    * выключаем сеть
    * разархивируем дистрибутив vscode-server на виртуальном хосте согласно 2-й инструкции
    * нажимаем _Retry_
1. Если вы все сделали верно, соединение будет успешно установлено (следите, чтобы сеть была отключена), и вы сможете выполнять команды в консоли и просматривать содержимое каталогов в виртуальном хосте
1. Залить архив с расширениями extensions.tar.gz на виртуальный хост
1. Разархивировать в папку ~/.vscode-server
1. Переконнектиться из VSCode к виртуальному хосту, проверить наличие расширений во вкладке EXTENSIONS -> SSH:
    * __Docker__
    * __Git Graph__
    * __Kubernetes__
    * __markdownlint__
    * __OpenShift Connector__
    * __Python__
    * __Scala (Metals)__
    * __YAML__
